# code_it_suisse

## Project setup
```
npm install
cd client
npm install
```

### Compiles and hot-reloads frontend for development
```
cd client
npm run serve
```

### Compiles and minifies for production
```
cd client
npm run build
```

### Run both front and backend
```
node app.js
```

### Run frontend tests
```
cd client
npm run test
```

### Lints and fixes files
```
cd client
npm run lint
```

### Build docker image
```
docker build -t dionang/test .
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
