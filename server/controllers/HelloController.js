module.exports = function (router) {
    router.get('/hello', function (req, res) {
        res.json({
            message: 'Hello World!'
        });
    });
}