FROM node:10-alpine

WORKDIR /

COPY client/dist client/dist

COPY server server

COPY package*.json ./

COPY app.js .

RUN npm install

EXPOSE 8080

CMD ["node", "app.js"]