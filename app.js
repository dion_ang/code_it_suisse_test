const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');
const app = express();
 
app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.json());

// serves static files from frontend, requires npm run build on client
// comment if just runnning backend
app.use(express.static(path.join(__dirname, 'client/dist')));

// adds all files in server/controllers to routes, with /api prefix
const router = express.Router()
const controllersPath = "./server/controllers/";
fs.readdirSync(controllersPath).forEach(function(file) {
    require(controllersPath+file)(router);
});
app.use("/api", router)
 
const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`listening on ${port}`);
});